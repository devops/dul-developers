FROM bitnami/apache:latest

ENV SHIBURL='https://shib.oit.duke.edu' \
    IDPXML='duke-metadata-2-signed.xml' \
    IDPCRT='idp_signing.crt'

USER 0

RUN install_packages vim
RUN install_packages wget
RUN install_packages rsync
RUN install_packages tar
RUN install_packages docker.io
RUN install_packages firewalld

EXPOSE 22
EXPOSE 3000
EXPOSE 5000
EXPOSE 8080
EXPOSE 8443

USER 1000
# CMD ["/bin/bash"]

